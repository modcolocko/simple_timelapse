import time
import os

DELAY=60 #Time between frames, 60 is one frame per minute
URL="INSERT URL HERE" #Ip address of photo
TIME=240 #Time in minutes

photos = int(round(TIME / (DELAY/60)))

print(f"Taking {photos} photos over {TIME} minutes")

for photo in range(photos):
	f = open("index.txt", "r")
	index = int(f.read())
	print(index)
	f.close()
	time.sleep(DELAY)
	os.system(f"curl {URL} --output images/{index}.jpg")
	f = open("index.txt", "w")
	index = index + 1
	f.write(str(index))
	f.close()
	
	
